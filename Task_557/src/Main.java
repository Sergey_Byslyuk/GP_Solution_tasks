import java.io.*;

public class Main {
    private static BufferedReader reader;
    private static PrintWriter pw;
    private static int[][] result;
    private static int array[] = new int[5];

    public static void main(String[] args) throws IOException {
        run();

    }

    private static void run() throws IOException {

        reader = new BufferedReader(new FileReader("resources/task_557/input.txt"));
        pw = new PrintWriter(new File("resources/task_557/output.txt"));

        String[] line = reader.readLine().split(" ");
        String[] line1 = reader.readLine().split(" ");

        array[0] = Integer.parseInt(line[0]); // number
        array[1] = Integer.parseInt(line[1]); // size
        array[2] = Integer.parseInt(line1[0]); // x
        array[3] = Integer.parseInt(line1[1]); //y
        array[4] = Integer.parseInt(reader.readLine()); //p

        read();
        multiplication();

        pw.println(result[array[2] - 1][array[3] - 1]);
        pw.close();

    }

    private static void read() throws IOException {
        reader.readLine();
        result = new int[array[1]][array[1]];
        for (int i = 0; i < array[1]; i++) {
            String[] line3 = reader.readLine().split(" ");
            for (int j = 0; j < array[1]; j++) {
                result[i][j] = Integer.parseInt(line3[j]);
            }
        }
        reader.readLine();
    }

    private static void multiplication() throws IOException {
        int[][] mat = new int[array[1]][array[1]];

        for (int counter = 1; counter < array[0]; counter++) {
            for (int i = 0; i < array[1]; i++) {
                String[] line4 = reader.readLine().split(" ");
                for (int j = 0; j < array[1]; j++) {
                    mat[i][j] = Integer.parseInt(line4[j]);
                }
            }
            reader.readLine();
            count(result, mat);
        }

    }

    private static void count(int[][] first, int[][] second) {
        int[][] last = new int[array[1]][array[1]];

        for (int j = 0; j < array[1]; j++) {
            for (int k = 0; k < array[1]; k++) {
                last[array[2] - 1][j] += first[array[2] - 1][k] * second[k][j];
                if (last[array[2] - 1][j] >= array[4]) {
                    last[array[2] - 1][j] = last[array[2] - 1][j] % array[4];
                }
            }

        }

        result = last;
    }


}
