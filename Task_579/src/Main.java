import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        new Main().run();
    }

    PrintWriter out;
    Scanner in;

    public void run() throws IOException {
        in = new Scanner(new File("resources/task_579/input.txt"));
        out = new PrintWriter(new File("resources/task_579/output.txt"));
        int n = in.nextInt();
        int c = 0;
        LinkedList<Integer> plus = new LinkedList<>();
        LinkedList<Integer> minus = new LinkedList<>();
        int sumP = 0;
        int sumM = 0;
        for (int i = 0; i < n; i++) {
            c = in.nextInt();
            if (c > 0) {
                plus.add(i + 1);
                sumP += c;
            }
            if (c < 0) {
                minus.add(i + 1);
                sumM += Math.abs(c);
            }
        }
        if (sumP > sumM) {
            out.print(plus.size() + "\n");
            for (int i = 0; i < plus.size(); i++) {
                out.print(plus.get(i) + " ");
            }
            out.close();
        } else {
            out.print(minus.size() + "\n");
            for (int i = 0; i < minus.size(); i++) {

                out.print(minus.get(i) + " ");
            }
            out.close();
        }
    }
}
