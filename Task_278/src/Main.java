import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    Scanner in;
    PrintWriter out;
    public static void main(String[] argv) throws FileNotFoundException {

        new Main().run();
    }

    public void run() throws FileNotFoundException {
        in = new Scanner(new File("resources/task_278/input.txt"));
        String s = in.nextLine(), t = in.nextLine() ;
        out = new PrintWriter(new File("resources/task_278/output.txt"));
        char arrayS[] = s.toCharArray();
        char arrayT[] = t.toCharArray();
        int h = 0;
        int k = arrayS.length;
        int z = arrayT.length;
        for (int i = 0; i < z ; i++) {
            if (h == k){
                out.print("YES");
                out.close();
                break;
            }
            if (arrayS[h] == arrayT[i]){
                h++;
            }
        }
        if (h == k){
            out.print("YES");
            out.close();
        }
        else {
            out.print("NO");
            out.close();
        }
    }
}