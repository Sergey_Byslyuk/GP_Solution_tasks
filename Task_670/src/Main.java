import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        new Main().run();
    }

    PrintWriter out;
    Scanner in;

    public void run() throws IOException {
        in = new Scanner(new File("resources/task_670/input.txt"));

        out = new PrintWriter(new File("resources/task_670/output.txt"));

        HashSet<Character> set = new HashSet<>();
        int n = in.nextInt();
        int  i = 0, z = 0;
        while (i != n) {
            z++;
            String si = Integer.toString(z);
            for (int j = 0; j < si.length(); j++) {
                set.add(si.charAt(j));
            }
            if(si.length() == set.size()){
                i++;
            }
            set.clear();
        }
        out.print(z);
        out.close();
    }

}
